# Alias must not contain dots. It is used for references inside the template.
profile = "bbeSpellablesInfra"
shared_credentials_file = "/Users/benoit/.aws/credentials"
region = "us-west-1"
domain = "www.spellables.org"
bucket = "prod-www.spellables.org"
