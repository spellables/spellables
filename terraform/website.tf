variable "profile" { }
variable "shared_credentials_file" { }
variable "region" { }
variable "domain" { }
variable "bucket" { }

provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.shared_credentials_file}"
  profile                 = "${var.profile}"
}

/*
provider "aws" {
  alias   = "east1"
  region  = "us-east-1"
}

data "aws_acm_certificate" "website_certificate" {
  provider = "aws.east1"
  domain   = "www.spellables.org"
  statuses = ["ISSUED"]
}
*/

resource "aws_s3_bucket" "website_bucket" {
  bucket = "${var.bucket}"
  acl = "public-read"
  policy = <<POLICY
{
  "Version":"2012-10-17",
  "Statement":[{
    "Sid":"PublicReadForGetBucketObjects",
        "Effect":"Allow",
      "Principal": "*",
      "Action":"s3:GetObject",
      "Resource":["arn:aws:s3:::${var.bucket}/*"
      ]
    }
  ]
}
POLICY

  website {
    index_document = "index.html"
    error_document = "404.html"
  }
}

resource "aws_cloudfront_distribution" "cdn" {
  depends_on = ["aws_s3_bucket.website_bucket"]
  origin {
    domain_name = "${var.bucket}.s3.amazonaws.com"
    origin_id = "website_bucket_origin"
    s3_origin_config {
      origin_access_identity = "" 
    }
  }
  enabled = true
  is_ipv6_enabled = true
  default_root_object = "index.html"
  aliases = ["${var.domain}"]
  price_class = "PriceClass_100"
  retain_on_delete = true
  default_cache_behavior {
    allowed_methods = [ "DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT" ]
    cached_methods = [ "GET", "HEAD" ]
    target_origin_id = "website_bucket_origin"
    forwarded_values {
      query_string = true
      cookies {
        forward = "none"
      }
    }
    viewer_protocol_policy = "allow-all"
    min_ttl = 0
    default_ttl = 3600
    max_ttl = 86400
  }
  viewer_certificate {
    //acm_certificate_arn = "${data.aws_acm_certificate.website_certificate.arn}"
    acm_certificate_arn = "arn:aws:acm:us-east-1:240993756291:certificate/ad0dd461-a113-4392-be53-d0b42d76e95a"
    ssl_support_method = "sni-only"
  }
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
}

output "cdn_domain_name" {
  value = ["${aws_cloudfront_distribution.cdn.domain_name}"]
}
