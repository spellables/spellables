# spellables

[![pipeline status](https://gitlab.com/spellables/spellables/badges/master/pipeline.svg)](https://gitlab.com/spellables/spellables/commits/master) [![coverage report](https://gitlab.com/spellables/spellables/badges/master/coverage.svg)](https://gitlab.com/spellables/spellables/commits/master)
[![New Coverage](https://sonarcloud.io/api/badges/measure?key=spellables_lib&metric=new_coverage)](https://sonarcloud.io/dashboard/index/spellables_lib)
[![New maintainability rating](https://sonarcloud.io/api/badges/measure?key=spellables_lib&metric=new_maintainability_rating)](https://sonarcloud.io/dashboard/index/spellables_lib)
[![New reliability rating](https://sonarcloud.io/api/badges/measure?key=spellables_lib&metric=new_reliability_rating)](https://sonarcloud.io/dashboard/index/spellables_lib)
[![New security rating](https://sonarcloud.io/api/badges/measure?key=spellables_lib&metric=new_security_rating)](https://sonarcloud.io/dashboard/index/spellables_lib)

Spellables is a project intended to transform any complex string into a spellable list of words and vice versa.

The goal of this project if to facilitate the oral/cognitive transmission of these complex strings.

A typical example is to copy/paste an hexadecimal token or a password between two computers without relying on any kind of physical connection for the transmission. This is especially useful when there is really no physical network or when the network is not trusted or when you have very limited rights on the systems.

## Examples

1a <=> fix educate abandon

1a234567 <=> object educate educate filter length ability

771416456d547f5ced086039e5494bf6 <=> object fruit choice gold repeat faint twist unfair drink attack skin net wild abandon address

1a2345671a2345671a2345671a2345671a2345671a2345671a2345671a234567 <=> object educate educate filter minimum minimum provide boss boss flush crowd crowd receive hammer hamster grow speed spend soda peasant pencil organ dutch earth day abandon account

## Development

### Library
The library is located in the 'lib' folder.

### Website development
The website (demo of library usage, UI above library, ...) is located in the 'web' folder.

To start a local webserver:
```
cd web
yarn install
yarn webpack-dev-server
```

The local webserver will be started and refresh automatically, served at http://localhost:8080/

## How it works

The system works by transforming the into a binary payload which is then transformed into a list of spellables words thanks to a dictionnary.

A checksum is embedded into the payload to detect any mistake.

The list of words is chosen so that at most only the first 4 letters of the words are needed indeed.

List of words will be available in various langages (not yet implemented).

The system will be made usable thanks to a website usable offline.

This inner working has been inspired by the work of [BIP-39](https://github.com/bitcoin/bips/blob/master/bip-0039.mediawiki), made for the Bitcoin blockchain. It is however totally independant.

## Payload format before transformation into list of words

GLOBAL_PAYLOAD = HEADER + ORIGINAL_PAYLOAD + PADDING

HEADER :
- 5 bits for size of padding 
- 8 bits for kind of payload

PADDING :
0 to 31 trailing zeros, ensuring that the global_payload is a multiple of 32 bits

Payload kinds :
1 : hex string, all lowercase