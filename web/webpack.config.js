const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const path = require('path');
const Clean = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    entry: {
        main: './app.js',
    },
    output: {
        path: path.resolve(__dirname, 'dist'), // output directory
        filename: '[name].[chunkhash].js', // name of the generated bundle
    },
    module: {
        rules: [{
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: 'css-loader',
                }),
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [{
                    loader: 'file-loader',
                    options: {},
                }],
            }
        ],
    },
    plugins: [
        new Clean([path.resolve(__dirname, 'dist')]),
        /*
        new BundleAnalyzerPlugin({
            analyzerMode: 'static'
        }),
        */
        new webpack.ProvidePlugin({
            $: 'jquery',
            Popper: ['popper.js', 'default']
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'node-static',
            filename: 'node-static.[chunkhash].js',
            minChunks(module, count) {
                var context = module.context;
                return context && context.indexOf('node_modules') >= 0;
            },
        }),
        new ExtractTextPlugin('style.[contenthash].css'),
        new HtmlWebpackPlugin({
            template: 'index.html',
            favicon: 'favicon.ico',
            inject: 'body',
        }),
        // new UglifyJsPlugin(),
    ],
};
