const spellables = require('./../lib/index.js');
// eslint-disable-next-line no-unused-vars

const bootstrapCss = require('bootstrap/dist/css/bootstrap.min.css');
const mainCss = require('./cover.css');

import 'bootstrap';

function encodeData() {
    $('#encode_error_alert').hide();
    try {
        const result = spellables.spell(dataToEncode);
        $('#encode_result').val(result);
    } catch (e) {
        console.log(e);
        $('#encode_result').val('');
        $('#encode_error_alert_message').text(e.message);
        $('#encode_error_alert').show();
    }
}

function decodeData() {
    $('#decode_error_alert').hide();
    try {
        const result = spellables.unspell(dataToDecode);
        $('#decode_result').val(result);
    } catch (e) {
        console.log(e);
        $('#decode_result').val('');
        $('#decode_error_alert_message').text(e.message);
        $('#decode_error_alert').show();
    }
}

let dataToEncode = '';
let dataToDecode = '';

function encodeBtnPressed() {
    if (dataToEncode !== $('#encode_data').val()) {
        dataToEncode = $('#encode_data').val();
        if (window.location.hostname === "www.spellables.org") { 
            _paq.push(['trackEvent', 'Button', 'Encode']);
        }
        encodeData();
    }
}

function decodeBtnPressed() {
    if (dataToDecode !== $('#decode_data').val()) {
        dataToDecode = $('#decode_data').val();
        if (window.location.hostname === "www.spellables.org") { 
            _paq.push(['trackEvent', 'Button', 'Decode']);
        }
        decodeData();
    }
}

$(function() {
    $('#encode-btn').click(function() {
        encodeBtnPressed();
    });
    $('#decode-btn').click(function() {
        decodeBtnPressed();
    });

    $('#encode_error_alert_btn').click(function() {
        $('#encode_error_alert').hide();
        $('#encode_data').val('');
    });
    $('#error_alert_btn').click(function() {
        $('#decode_error_alert').hide();
        $('#decode_data').val('');
    });

    $("#encode_data").keypress(function(e) {
        if(e.which == 13) {
            encodeBtnPressed();
        }
    });
    $("#decode_data").keypress(function(e) {
        if(e.which == 13) {
            decodeBtnPressed();
        }
    });

});