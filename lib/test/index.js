'use strict';
const should = require('chai').should(); // eslint-disable-line no-unused-vars
const expect = require('chai').expect;
const spellable = require('../index');
const spell = spellable.spell;
const unspell = spellable.unspell;
const wordlists = spellable.wordlists;
const canul2bin = spellable.convertAlphaNumUpperAndLowerToBinary;
const bin2canul = spellable.convertBinaryToAlphaNumUpperAndLower;
const ascii2bin = spellable.convertAsciiToBinary;
const bin2ascii = spellable.convertBinaryToAscii;
const asciiext2bin = spellable.convertAsciiExtendedToBinary;
const bin2asciiext = spellable.convertBinaryToAsciiExtended;

describe('#spell and #unspell', function() {
    it('spell and unspell "1a" into original value', function() {
        unspell(spell('1a')).should.equal('1a');
    });
    it('spell and unspell "1a234567" into original value', function() {
        unspell(spell('1a234567')).should.equal('1a234567');
    });
    it('spell and unspell "771416456d547f5ced086039e5494bf6"' +
        ' into original value', function() {
        unspell(spell('771416456d547f5ced086039e5494bf6'))
            .should.equal('771416456d547f5ced086039e5494bf6');
    });
    it('spell and unspell ' +
        '"1a2345671a2345671a2345671a2345671a2345671a2345671a2345671a234567"' +
        ' into original value', function() {
        unspell(spell(
            '1a2345671a2345671a2345671a2345671a2345671a2345671a2345671a234567'
            )).should.equal(
            '1a2345671a2345671a2345671a2345671a2345671a2345671a2345671a234567');
    });
    it('spell and unspell "1A" into original value', function() {
        unspell(spell('1A')).should.equal('1A');
    });
    it('spell and unspell "1A2345CB67FF" into original value', function() {
        unspell(spell('1A2345CB67FF')).should.equal('1A2345CB67FF');
    });
    it('spell and unspell "1A2b" into original value', function() {
        unspell(spell('1A2b')).should.equal('1A2b');
    });
    it('spell and unspell "1A2" into original value', function() {
        unspell(spell('1A2')).should.equal('1A2');
    });    
    it('spell and unspell "1A2#b" into original value', function() {
        unspell(spell('1A2#b')).should.equal('1A2#b');
    });     
    it('spell and unspell "1A2üb" into original value', function() {
        unspell(spell('1A2üb')).should.equal('1A2üb');
    });
    it('spell and unspell "đ" into original value', function() {
        unspell(spell('đ')).should.equal('đ');
    });
    it('spell and unspell "Ͽ" into original value', function() {
        unspell(spell('Ͽ')).should.equal('Ͽ');
    });
    it('spell and unspell "Օ" into original value', function() {
        unspell(spell('Օ')).should.equal('Օ');
    });
    it('spell and unspell "ঈ" into original value', function() {
        unspell(spell('ঈ')).should.equal('ঈ');
    });
    it('spell and unspell "က" into original value', function() {
        unspell(spell('က')).should.equal('က');
    });
    it('spell and unspell "•" into original value', function() {
        unspell(spell('•')).should.equal('•');
    });
    it('spell and unspell "䀁" into original value', function() {
        unspell(spell('䀁')).should.equal('䀁');
    });
    it('spell and unspell "退" into original value', function() {
        unspell(spell('退')).should.equal('退');
    });
    it('unspell "garment shoot ability" should be unsupported kind',
        function() {
        expect(() => unspell('garment shoot ability')).to.throw('Unsupported');
    });
    it('unspell "garment shoot" should be invalid',
        function() {
        expect(() => unspell('garment shoot')).to.throw('Invalid');
    });
    it('unspell "fix educate ability" should be invalid checksum',
        function() {
        expect(() => unspell('fix educate ability')).to.throw('checksum');
    });
    it('unspell "fix educate abbbbaaaadddooon" should be valid', 
        function() {
        expect(() => unspell('fix educate abbbbaaaadddooon'))
            .to.throw('Invalid');   
    });
    it('unspell "fix educate abbadon" should be valid', function() {
        unspell('fix educate abbadon').should.equal('1a');
    });
    it('unspell "fix educate abbanndon" should be valid', function() {
        unspell('fix educate abbanndon').should.equal('1a');
    });
    it('unspell "fix educate qbqdin" should be invalid',
        function() {
        expect(() => unspell('fix educate qbqdin'))
            .to.throw('Invalid');
    });
    it('unspell "fix educate fgzttrrwq" should be invalid',
        function() {
        expect(() => unspell('fix educate fgzttrrwq'))
            .to.throw('Invalid');
    });

});

describe('#wordlists', function() {
    it('french wordlists must has correct length', function() {
        wordlists.french.length.should.equal(2048);
    });
    it('english wordlists must has correct length', function() {
        wordlists.english.length.should.equal(2048);
    });
    it('chinese_simplified wordlists must has correct length', function() {
        wordlists.chineseSimplified.length.should.equal(2048);
    });
    it('chinese_traditional wordlists must has correct length', function() {
        wordlists.chineseTraditional.length.should.equal(2048);
    });
    it('italian wordlists must has correct length', function() {
        wordlists.italian.length.should.equal(2048);
    });
    it('japanese wordlists must has correct length', function() {
        wordlists.japanese.length.should.equal(2048);
    });
    it('spanish wordlists must has correct length', function() {
        wordlists.spanish.length.should.equal(2048);
    });
});

describe('#convertAlphaNumUpperAndLowerToBinary', function() {
    it('convert "0" to "000000"', function() {
        canul2bin('0').should.equal('000000');
    })
    it('convert "00" to "000000000000"', function() {
        canul2bin('00').should.equal('000000000000');
    })
    it('convert "1" to "000001"', function() {
        canul2bin('1').should.equal('000001');
    })
    it('convert "11" to "000001000001"', function() {
        canul2bin('11').should.equal('000001000001');
    })
    it('convert "a" to "100100"', function() {
        canul2bin('a').should.equal('100100');
    })
    it('convert "Z" to "100011"', function() {
        canul2bin('Z').should.equal('100011');
    })
    it('convert "aZ" to "100100100011"', function() {
        canul2bin('aZ').should.equal('100100100011');
    })  
    it('spell "#" should be unexpected char', function() {
        expect(() => canul2bin('#')).to.throw('Unexpected');
    });  
    it('spell "ü" should be unexpected char', function() {
        expect(() => canul2bin('ü')).to.throw('Unexpected');
    }); 
    it('spell "袈" should be unexpected char', function() {
        expect(() => canul2bin('袈')).to.throw('Unexpected');
    }); 
    it('spell "1A2袈b" should be unexpected char', function() {
        expect(() => canul2bin('1A2袈b')).to.throw('Unexpected');
    }); 
})

describe('#convertBinaryToAlphaNumUpperAndLower', function() {
    it('convert "000000" to "0"', function() {
        bin2canul('000000').should.equal('0');
    })
    it('convert "000000000000" to "00"', function() {
        bin2canul('000000000000').should.equal('00');
    })
    it('convert "000001" to "1"', function() {
        bin2canul('000001').should.equal('1');
    })
    it('convert "000001000001" to "11"', function() {
        bin2canul('000001000001').should.equal('11');
    })
    it('convert "100100" to "a"', function() {
        bin2canul('100100').should.equal('a');
    })
    it('convert "100011" to "Z"', function() {
        bin2canul('100011').should.equal('Z');
    })
    it('convert "100100100011" to "aZ"', function() {
        bin2canul('100100100011').should.equal('aZ');
    })
})

describe('#convertAsciiToBinary', function() {
    it('convert "0" to "0110000"', function() {
        ascii2bin('0').should.equal('0110000');
    })
    it('convert "00" to "01100000110000"', function() {
        ascii2bin('00').should.equal('01100000110000');
    })
    it('convert "1" to "0110001"', function() {
        ascii2bin('1').should.equal('0110001');
    })
    it('convert "11" to "01100010110001"', function() {
        ascii2bin('11').should.equal('01100010110001');
    })
    it('convert "a" to "1100001"', function() {
        ascii2bin('a').should.equal('1100001');
    })
    it('convert "Z" to "1011010"', function() {
        ascii2bin('Z').should.equal('1011010');
    })
    it('convert "#" to "0100011"', function() {
        ascii2bin('#').should.equal('0100011');
    })
    it('convert "aZ" to "11000011011010"', function() {
        ascii2bin('aZ').should.equal('11000011011010');
    })
    it('spell "ü" should be unexpected char', function() {
        expect(() => ascii2bin('ü')).to.throw('Unexpected');
    }); 
    it('spell "袈" should be unexpected char', function() {
        expect(() => ascii2bin('袈')).to.throw('Unexpected');
    }); 
    it('spell "1A2袈b" should be unexpected char', function() {
        expect(() => ascii2bin('1A2袈b')).to.throw('Unexpected');
    }); 
})

describe('#convertBinaryToAscii', function() {
    it('convert "0110000" to "0"', function() {
        bin2ascii('0110000').should.equal('0');
    })
    it('convert "01100000110000" to "00"', function() {
        bin2ascii('01100000110000').should.equal('00');
    })
    it('convert "0110001" to "1"', function() {
        bin2ascii('0110001').should.equal('1');
    })
    it('convert "01100010110001" to "11"', function() {
        bin2ascii('01100010110001').should.equal('11');
    })
    it('convert "1100001" to "a"', function() {
        bin2ascii('1100001').should.equal('a');
    })
    it('convert "1011010" to "Z"', function() {
        bin2ascii('1011010').should.equal('Z');
    })
    it('convert "0100011" to "#"', function() {
        bin2ascii('0100011').should.equal('#');
    })
    it('convert "11000011011010" to "aZ"', function() {
        bin2ascii('11000011011010').should.equal('aZ');
    })
})

describe('#convertAsciiExtendedToBinary', function() {
    it('convert "0" to "00110000"', function() {
        asciiext2bin('0').should.equal('00110000');
    })
    it('convert "00" to "0011000000110000"', function() {
        asciiext2bin('00').should.equal('0011000000110000');
    })
    it('convert "1" to "00110001"', function() {
        asciiext2bin('1').should.equal('00110001');
    })
    it('convert "11" to "0011000100110001"', function() {
        asciiext2bin('11').should.equal('0011000100110001');
    })
    it('convert "a" to "01100001"', function() {
        asciiext2bin('a').should.equal('01100001');
    })
    it('convert "Z" to "01011010"', function() {
        asciiext2bin('Z').should.equal('01011010');
    })
    it('convert "#" to "00100011"', function() {
        asciiext2bin('#').should.equal('00100011');
    })
    it('convert "aZ" to "0110000101011010"', function() {
        asciiext2bin('aZ').should.equal('0110000101011010');
    })
    it('convert "ü" to "11111100"', function() {
        asciiext2bin('ü').should.equal('11111100');
    })
    it('spell "袈" should be unexpected char', function() {
        expect(() => asciiext2bin('袈')).to.throw('Unexpected');
    }); 
    it('spell "1A2袈b" should be unexpected char', function() {
        expect(() => asciiext2bin('1A2袈b')).to.throw('Unexpected');
    }); 
})

describe('#convertBinaryToAsciiExtended', function() {
    it('convert "00110000" to "0"', function() {
        bin2asciiext('00110000').should.equal('0');
    })
    it('convert "0011000000110000" to "00"', function() {
        bin2asciiext('0011000000110000').should.equal('00');
    })
    it('convert "00110001" to "1"', function() {
        bin2asciiext('00110001').should.equal('1');
    })
    it('convert "0011000100110001" to "11"', function() {
        bin2asciiext('0011000100110001').should.equal('11');
    })
    it('convert "01100001" to "a"', function() {
        bin2asciiext('01100001').should.equal('a');
    })
    it('convert "01011010" to "Z"', function() {
        bin2asciiext('01011010').should.equal('Z');
    })
    it('convert "00100011" to "#"', function() {
        bin2asciiext('00100011').should.equal('#');
    })
    it('convert "0110000101011010" to "aZ"', function() {
        bin2asciiext('0110000101011010').should.equal('aZ');
    })
    it('convert "11111100" to "ü"', function() {
        bin2asciiext('11111100').should.equal('ü');
    })
})

describe('#convertUnicode9ToBinary', function() {
    it('convert "đ" to "100010001"', function() {
        spellable.convertUnicode9ToBinary('đ').should.equal('100010001');
    })
    it('spell "Ͽ" should be unexpected char', function() {
        expect(() => spellable.convertUnicode9ToBinary('Օ')).to.throw('Unexpected');
    }); 
    it('spell "Օ" should be unexpected char', function() {
        expect(() => spellable.convertUnicode9ToBinary('Օ')).to.throw('Unexpected');
    }); 
    it('spell "ঈ" should be unexpected char', function() {
        expect(() => spellable.convertUnicode9ToBinary('ঈ')).to.throw('Unexpected');
    }); 
    it('spell "က" should be unexpected char', function() {
        expect(() => spellable.convertUnicode9ToBinary('က')).to.throw('Unexpected');
    }); 
    it('spell "•" should be unexpected char', function() {
        expect(() => spellable.convertUnicode9ToBinary('•')).to.throw('Unexpected');
    }); 
    it('spell "䀁" should be unexpected char', function() {
        expect(() => spellable.convertUnicode9ToBinary('䀁')).to.throw('Unexpected');
    }); 
    it('spell "退" should be unexpected char', function() {
        expect(() => spellable.convertUnicode9ToBinary('退')).to.throw('Unexpected');
    });
})

describe('#convertBinaryToUnicode9', function() {
    it('convert "100010001" to "đ"', function() {
        spellable.convertBinaryToUnicode9('100010001').should.equal('đ');
    })
})

describe('#convertUnicode10ToBinary', function() {
    it('convert "Ͽ" to "1111111111"', function() {
        spellable.convertUnicode10ToBinary('Ͽ').should.equal('1111111111');
    })
    it('spell "Օ" should be unexpected char', function() {
        expect(() => spellable.convertUnicode10ToBinary('Օ')).to.throw('Unexpected');
    }); 
    it('spell "ঈ" should be unexpected char', function() {
        expect(() => spellable.convertUnicode10ToBinary('ঈ')).to.throw('Unexpected');
    }); 
    it('spell "က" should be unexpected char', function() {
        expect(() => spellable.convertUnicode10ToBinary('က')).to.throw('Unexpected');
    }); 
    it('spell "•" should be unexpected char', function() {
        expect(() => spellable.convertUnicode10ToBinary('•')).to.throw('Unexpected');
    }); 
    it('spell "䀁" should be unexpected char', function() {
        expect(() => spellable.convertUnicode10ToBinary('䀁')).to.throw('Unexpected');
    }); 
    it('spell "退" should be unexpected char', function() {
        expect(() => spellable.convertUnicode10ToBinary('退')).to.throw('Unexpected');
    }); 
})

describe('#convertBinaryToUnicode10', function() {
    it('convert "1111111111" to "Ͽ"', function() {
        spellable.convertBinaryToUnicode10('1111111111').should.equal('Ͽ');
    })
})

describe('#convertUnicode11ToBinary', function() {
    it('convert "Օ" to "10101010101"', function() {
        spellable.convertUnicode11ToBinary('Օ').should.equal('10101010101');
    })
    it('spell "ঈ" should be unexpected char', function() {
        expect(() => spellable.convertUnicode11ToBinary('ঈ')).to.throw('Unexpected');
    }); 
    it('spell "က" should be unexpected char', function() {
        expect(() => spellable.convertUnicode11ToBinary('က')).to.throw('Unexpected');
    }); 
    it('spell "•" should be unexpected char', function() {
        expect(() => spellable.convertUnicode11ToBinary('•')).to.throw('Unexpected');
    }); 
    it('spell "䀁" should be unexpected char', function() {
        expect(() => spellable.convertUnicode11ToBinary('䀁')).to.throw('Unexpected');
    }); 
    it('spell "退" should be unexpected char', function() {
        expect(() => spellable.convertUnicode11ToBinary('退')).to.throw('Unexpected');
    }); 
})

describe('#convertBinaryToUnicode11', function() {
    it('convert "10101010101" to "Օ"', function() {
        spellable.convertBinaryToUnicode11('10101010101').should.equal('Օ');
    })
})

describe('#convertUnicode12ToBinary', function() {
    it('convert "ঈ" to "1000000000000"', function() {
        spellable.convertUnicode12ToBinary('ঈ').should.equal('100110001000');
    })
    it('spell "က" should be unexpected char', function() {
        expect(() => spellable.convertUnicode12ToBinary('က')).to.throw('Unexpected');
    }); 
    it('spell "•" should be unexpected char', function() {
        expect(() => spellable.convertUnicode12ToBinary('•')).to.throw('Unexpected');
    }); 
    it('spell "䀁" should be unexpected char', function() {
        expect(() => spellable.convertUnicode12ToBinary('䀁')).to.throw('Unexpected');
    }); 
    it('spell "退" should be unexpected char', function() {
        expect(() => spellable.convertUnicode12ToBinary('退')).to.throw('Unexpected');
    }); 
})

describe('#convertBinaryToUnicode12', function() {
    it('convert "100110001000" to "ঈ"', function() {
        spellable.convertBinaryToUnicode12('100110001000').should.equal('ঈ');
    })
})

describe('#convertUnicode13ToBinary', function() {
    it('convert "က" to "1000000000000"', function() {
        spellable.convertUnicode13ToBinary('က').should.equal('1000000000000');
    })
    it('spell "•" should be unexpected char', function() {
        expect(() => spellable.convertUnicode13ToBinary('•')).to.throw('Unexpected');
    }); 
    it('spell "䀁" should be unexpected char', function() {
        expect(() => spellable.convertUnicode13ToBinary('䀁')).to.throw('Unexpected');
    }); 
    it('spell "退" should be unexpected char', function() {
        expect(() => spellable.convertUnicode13ToBinary('退')).to.throw('Unexpected');
    }); 
})

describe('#convertBinaryToUnicode13', function() {
    it('convert "1000000000000" to "က"', function() {
        spellable.convertBinaryToUnicode13('1000000000000').should.equal('က');
    })
})

describe('#convertUnicode14ToBinary', function() {
    it('convert "•" to "10000000100010"', function() {
        spellable.convertUnicode14ToBinary('•').should.equal('10000000100010');
    })
    it('spell "䀁" should be unexpected char', function() {
        expect(() => spellable.convertUnicode14ToBinary('䀁')).to.throw('Unexpected');
    }); 
    it('spell "退" should be unexpected char', function() {
        expect(() => spellable.convertUnicode14ToBinary('退')).to.throw('Unexpected');
    }); 
})

describe('#convertBinaryToUnicode14', function() {
    it('convert "10000000100010" to "•"', function() {
        spellable.convertBinaryToUnicode14('10000000100010').should.equal('•');
    })
})

describe('#convertUnicode15ToBinary', function() {
    it('convert "䀁" to "100000000000001"', function() {
        spellable.convertUnicode15ToBinary('䀁').should.equal('100000000000001');
    })
    it('spell "退" should be unexpected char', function() {
        expect(() => spellable.convertUnicode15ToBinary('退')).to.throw('Unexpected');
    }); 
})

describe('#convertBinaryToUnicode15', function() {
    it('convert "100000000000001" to "䀁"', function() {
        spellable.convertBinaryToUnicode15('100000000000001').should.equal('䀁');
    })
})

describe('#convertUnicode16ToBinary', function() {
    it('convert "退" to "1001000000000000"', function() {
        spellable.convertUnicode16ToBinary('退').should.equal('1001000000000000');
    })
})

describe('#convertBinaryToUnicode16', function() {
    it('convert "1001000000000000" to "退"', function() {
        spellable.convertBinaryToUnicode16('1001000000000000').should.equal('退');
    })
})