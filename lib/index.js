'use strict';
const Buffer = require('safe-buffer').Buffer;
const createHash = require('create-hash');
const stringSimilarity = require('string-similarity');

// use unorm until String.prototype.normalize gets better browser support
const unorm = require('unorm');

const CHINESE_SIMPLIFIED_WORDLIST =
    require('./wordlists/chinese_simplified.json');
const CHINESE_TRADITIONAL_WORDLIST =
    require('./wordlists/chinese_traditional.json');
const ENGLISH_WORDLIST = require('./wordlists/english.json');
const FRENCH_WORDLIST = require('./wordlists/french.json');
const ITALIAN_WORDLIST = require('./wordlists/italian.json');
const JAPANESE_WORDLIST = require('./wordlists/japanese.json');
const SPANISH_WORDLIST = require('./wordlists/spanish.json');
const DEFAULT_WORDLIST = ENGLISH_WORDLIST;

const INVALID_MNEMONIC = 'Invalid mnemonic';
const INVALID_CHECKSUM = 'Invalid mnemonic checksum';
const UNSUPPORTED_INPUT_FORMAT = 'Unsupported input format';
const UNSUPPORTED_KIND = 'Unsupported kind';
const INCORRECT_KIND = 'Incorrect kind passed';
const UNEXPECTED_CHAR = 'Unexpected character encountered'

const HEXASTRING_LCASE = 1;
const HEXASTRING_UCASE = 2;
const ALPHANUMSTRING_ALLCASE = 3;
const ASCIISTRING = 4;
const ASCIIEXTENDEDSTRING = 5;
const UNICODE9STRING = 6;
const UNICODE10STRING = 7;
const UNICODE11STRING = 8;
const UNICODE12STRING = 9;
const UNICODE13STRING = 10;
const UNICODE14STRING = 11;
const UNICODE15STRING = 12;
const UNICODE16STRING = 13;

function lpad(str, padString, length) {
    while (str.length < length) {
        str = padString + str;
    }
    return str;
}

function binaryToByte(bin) {
    return parseInt(bin, 2);
}

function bytesToBinary(bytes) {
    return bytes.map(function(x) {
      return lpad(x.toString(2), '0', 8);
    }).join('');
}

function binaryToHexBuffer(bin) {
    const hexBytes = bin.match(/(.{1,8})/g).map(binaryToByte);
    const hex = Buffer.from(hexBytes);
    return hex;
}

function binaryToHex(bin) {
    return bin.match(/(.{1,4})/g).map(function(a) {
        return binaryToByte(a).toString(16);
    })
    .reduce(function(a, b) {
        return a + b;
    }, '');
}

function deriveChecksumBits(payloadBits) {
    const ENT = payloadBits.length;
    const CS = ENT / 32;
    const hash = createHash('sha256')
        .update(binaryToHexBuffer(payloadBits)).digest();
    return bytesToBinary([].slice.call(hash)).slice(0, CS);
}

/*
function convertToCharCodes(str) {
    result = [];
    for(var i = 0, length = str.length; i < length; i++) {
        result.push(str.charCodeAt(i));
    }
    return result;
}
*/

function spellHexaString(hexStr, kind) {
    if (kind !== HEXASTRING_LCASE && kind !== HEXASTRING_UCASE) {
        throw new Error(INCORRECT_KIND);
    }
    hexStr = Buffer.from(hexStr, 'hex');
    const hexBits = bytesToBinary([].slice.call(hexStr));
    return encodeToMnemonic(kind, hexBits);
}

function convertAlphaNumUpperAndLowerToBinary(str) {
    const bits = str.split('').map(function(character) {
        const res = character.charCodeAt(0);
        return res;
    }).map(function(code) {
        let res = 0;
        if (code >= 48 && code <= 57) {
            res = code - 48;
        } else if (code >= 65 && code <= 90) {
            res = code - 55;
        } else if (code >= 97 && code <= 122) {
            res = code - 61;
        } else {
            throw new Error(UNEXPECTED_CHAR);
        }
        return res;
    }).map(function(code) {
        const res = lpad(code.toString(2), '0', 6);
        return res;
    }).reduce(function(a, b) {
        return a + b;
    }, '');
    return bits;
}

function convertBinaryToAlphaNumUpperAndLower(bits) {
    const items = bits.match(/.{1,6}/g);
    const strCodes = items.map(function(item) {
        const res = parseInt(item, 2);
        return res;
    }).map(function(code) {
        let res;
        if (code >= 0 && code <= 9) {
            res = code + 48;
        } else if (code >= 10 && code <= 35) {
            res = code + 55;
        } else if (code >= 36 && code <= 61) {
            res = code + 61;
        } else {
            throw new Error(UNEXPECTED_CHAR);
        }
        return res;
    });
    const str = String.fromCharCode(...strCodes)
    return str;
}

function spellAlphaNumUpperAndLowerString(str) {
    const bits = convertAlphaNumUpperAndLowerToBinary(str);
    return encodeToMnemonic(ALPHANUMSTRING_ALLCASE, bits);
}

function spellAsciiString(str) {
    const bits = convertCharCodeToBinary(str, 7);
    return encodeToMnemonic(ASCIISTRING, bits);
}

function spellAsciiExtendedString(str) {
    const bits = convertCharCodeToBinary(str, 8);
    return encodeToMnemonic(ASCIIEXTENDEDSTRING, bits);
}

function convertCharCodeToBinary(str, nbBits) {
    const bits = str.split('').map(function(character) {
        const res = character.charCodeAt(0);
        return res;
    }).map(function(code) {
        if (code >= Math.pow(2, nbBits)) {
            throw new Error(UNEXPECTED_CHAR);
        }
        return code;
    }).map(function(code) {
        const res = lpad(code.toString(2), '0', nbBits);
        return res;
    }).reduce(function(a, b) {
        return a + b;
    }, '');
    return bits;
}

function convertBinaryToCharCode(bits, nbBits) {
    const regExp = new RegExp(".{1," + nbBits + "}", "g");
    const items = bits.match(regExp);;
    const strCodes = items.map(function(item) {
        const res = parseInt(item, 2);
        return res;
    });
    const str = String.fromCharCode(...strCodes)
    return str;
}

function spellUnicode9String(str) {
    const bits = convertCharCodeToBinary(str, 9);
    return encodeToMnemonic(UNICODE9STRING, bits);
}

function spellUnicode10String(str) {
    const bits = convertCharCodeToBinary(str, 10);
    return encodeToMnemonic(UNICODE10STRING, bits);
}

function spellUnicode11String(str) {
    const bits = convertCharCodeToBinary(str, 11);
    return encodeToMnemonic(UNICODE11STRING, bits);
}

function spellUnicode12String(str) {
    const bits = convertCharCodeToBinary(str, 12);
    return encodeToMnemonic(UNICODE12STRING, bits);
}

function spellUnicode13String(str) {
    const bits = convertCharCodeToBinary(str, 13);
    return encodeToMnemonic(UNICODE13STRING, bits);
}

function spellUnicode14String(str) {
    const bits = convertCharCodeToBinary(str, 14);
    return encodeToMnemonic(UNICODE14STRING, bits);
}

function spellUnicode15String(str) {
    const bits = convertCharCodeToBinary(str, 15);
    return encodeToMnemonic(UNICODE15STRING, bits);
}

function spellUnicode16String(str) {
    const bits = convertCharCodeToBinary(str, 16);
    return encodeToMnemonic(UNICODE16STRING, bits);
}

function encodeToMnemonic(payloadKind, originalPayloadBits, wordlist) {
    wordlist = wordlist || DEFAULT_WORDLIST;
    let paddingLength = 32 - ((originalPayloadBits.length + 5 + 8) % 32);
    if (paddingLength === 32) {
        paddingLength = 0;
    }
    const paddingBits = lpad('', '0', paddingLength);
    const headerBits = lpad(paddingLength.toString(2), '0', 5)
        + lpad(payloadKind.toString(2), '0', 8);
    const globalPayloadBits = headerBits + originalPayloadBits + paddingBits;
    const checksumBits = deriveChecksumBits(globalPayloadBits);
    const bits = globalPayloadBits + checksumBits;
    const chunks = bits.match(/(.{1,11})/g);
    const words = chunks.map(function(binary) {
        const index = binaryToByte(binary);
        return wordlist[index];
    });
    return wordlist === JAPANESE_WORDLIST ? words.join('\u3000') :
        words.join(' ');
}

function decodeFromMnemonic(mnemonic, wordlist) {
    wordlist = wordlist || DEFAULT_WORDLIST;
    const words = unorm.nfkd(mnemonic).split(' ');
    if (words.length % 3 !== 0) {
        throw new Error(INVALID_MNEMONIC);
    }
    const exactWords = words.map(function(word) {
        const matches = stringSimilarity.findBestMatch(word, wordlist);
        const exactWord = matches.bestMatch.target;
        if (matches.bestMatch.rating < 0.5) {
            if (exactWord !== word) {
                console.log('\'' + word + '\' has not been replaced by \'' + exactWord + '\'')
                console.log(JSON.stringify(matches.bestMatch))
            }
            return word;
        } else {
            if (exactWord !== word) {
                console.log('\'' + word + '\' has  been replaced by \'' + exactWord + '\'')
                console.log(JSON.stringify(matches.bestMatch))
            }
            return exactWord;
        }
    })
    // convert word indices to 11 bit binary strings
    const bits = exactWords.map(function(word) {
        const index = wordlist.indexOf(word);
      if (index === -1) {
          throw new Error(INVALID_MNEMONIC);
      }

      return lpad(index.toString(2), '0', 11);
    }).join('');
    // split the binary string into ENT/CS
    const dividerIndex = Math.floor(bits.length / 33) * 32;
    const hexBits = bits.slice(0, dividerIndex);
    const checksumBits = bits.slice(dividerIndex);
    // calculate the checksum and compare
    const newChecksum = deriveChecksumBits(hexBits);
    if (newChecksum !== checksumBits) {
        throw new Error(INVALID_CHECKSUM);
    }
    const padding = parseInt(hexBits.slice(0, 5), 2);
    const kind = parseInt(hexBits.slice(5, 13), 2);
    const payload = hexBits.slice(13, hexBits.length - padding);
    return {
        payload: payload,
        kind: kind,
    };
}

function spell(str) {
    if (/^(?:[0-9a-f]{2})*$/.test(str)) {
        // Lowercase Hexadecimal
        return spellHexaString(str, HEXASTRING_LCASE);
    } else if (/^(?:[0-9A-F]{2})*$/.test(str)) {
        // Lowercase Hexadecimal
        return spellHexaString(str, HEXASTRING_UCASE);
    } else if (/^(?:[A-Za-z0-9])*$/.test(str)) {
        // AlphaNumeric upper and lower case
        return spellAlphaNumUpperAndLowerString(str);
    } else if (/^(?:[\x00-\x7F])*$/.test(str)) {
        // AlphaNumeric upper and lower case
        return spellAsciiString(str);
    } else if (/^(?:[\x00-\xFF])*$/.test(str)) {
        // AlphaNumeric upper and lower case
        return spellAsciiExtendedString(str);
    } else if (/^(?:[\u0000-\u01FF])*$/.test(str)) {
        // AlphaNumeric upper and lower case
        return spellUnicode9String(str);
    } else if (/^(?:[\u0000-\u03FF])*$/.test(str)) {
        // AlphaNumeric upper and lower case
        return spellUnicode10String(str);
    } else if (/^(?:[\u0000-\u07FF])*$/.test(str)) {
        // AlphaNumeric upper and lower case
        return spellUnicode11String(str);
    } else if (/^(?:[\u0000-\u0FFF])*$/.test(str)) {
        // AlphaNumeric upper and lower case
        return spellUnicode12String(str);
    } else if (/^(?:[\u0000-\u1FFF])*$/.test(str)) {
        // AlphaNumeric upper and lower case
        return spellUnicode13String(str);
    } else if (/^(?:[\u0000-\u3FFF])*$/.test(str)) {
        // AlphaNumeric upper and lower case
        return spellUnicode14String(str);
    } else if (/^(?:[\u0000-\u7FFF])*$/.test(str)) {
        // AlphaNumeric upper and lower case
        return spellUnicode15String(str);
    } else if (/^(?:[\u0000-\uFFFF])*$/.test(str)) {
        // AlphaNumeric upper and lower case
        return spellUnicode16String(str);
    } else {
        // TODO : do whatever else you can do.
        throw new Error(UNSUPPORTED_INPUT_FORMAT);
    }
}

function unspell(str) {
    const res = decodeFromMnemonic(str);
    // TODO : detect that str is an HexaString
    if (res.kind === HEXASTRING_LCASE) {
        return binaryToHex(res.payload);
    } else if (res.kind === HEXASTRING_UCASE) {
        return binaryToHex(res.payload).toUpperCase();
    } else if (res.kind === ALPHANUMSTRING_ALLCASE) {
        return convertBinaryToAlphaNumUpperAndLower(res.payload);
    } else if (res.kind === ASCIISTRING) {
        return convertBinaryToCharCode(res.payload, 7);
    } else if (res.kind === ASCIIEXTENDEDSTRING) {
        return convertBinaryToCharCode(res.payload, 8);
    } else if (res.kind === UNICODE9STRING) {
        return convertBinaryToCharCode(res.payload, 9);
    } else if (res.kind === UNICODE10STRING) {
        return convertBinaryToCharCode(res.payload, 10);
    } else if (res.kind === UNICODE11STRING) {
        return convertBinaryToCharCode(res.payload, 11);
    } else if (res.kind === UNICODE12STRING) {
        return convertBinaryToCharCode(res.payload, 12);
    } else if (res.kind === UNICODE13STRING) {
        return convertBinaryToCharCode(res.payload, 13);
    } else if (res.kind === UNICODE14STRING) {
        return convertBinaryToCharCode(res.payload, 14);
    } else if (res.kind === UNICODE15STRING) {
        return convertBinaryToCharCode(res.payload, 15);
    } else if (res.kind === UNICODE16STRING) {
        return convertBinaryToCharCode(res.payload, 16);
    } else {
        // TODO : do whatever else you can do.
        throw new Error(UNSUPPORTED_KIND);
    }
}

module.exports = {
    spell: spell,
    unspell: unspell,
    wordlists: {
        chineseSimplified: CHINESE_SIMPLIFIED_WORDLIST,
        chineseTraditional: CHINESE_TRADITIONAL_WORDLIST,
        english: ENGLISH_WORDLIST,
        french: FRENCH_WORDLIST,
        italian: ITALIAN_WORDLIST,
        japanese: JAPANESE_WORDLIST,
        spanish: SPANISH_WORDLIST,
    },
    convertAlphaNumUpperAndLowerToBinary: convertAlphaNumUpperAndLowerToBinary,
    convertBinaryToAlphaNumUpperAndLower: convertBinaryToAlphaNumUpperAndLower,
    convertAsciiToBinary: function(x) { return convertCharCodeToBinary(x, 7);},
    convertBinaryToAscii: function(x) { return convertBinaryToCharCode(x, 7);},
    convertAsciiExtendedToBinary: function(x) { return convertCharCodeToBinary(x, 8);},
    convertBinaryToAsciiExtended: function(x) { return convertBinaryToCharCode(x, 8);},
    convertUnicode9ToBinary: function(x) { return convertCharCodeToBinary(x, 9);},
    convertBinaryToUnicode9: function(x) { return convertBinaryToCharCode(x, 9);},
    convertUnicode10ToBinary: function(x) { return convertCharCodeToBinary(x, 10);},
    convertBinaryToUnicode10: function(x) { return convertBinaryToCharCode(x, 10);},
    convertUnicode11ToBinary: function(x) { return convertCharCodeToBinary(x, 11);},
    convertBinaryToUnicode11: function(x) { return convertBinaryToCharCode(x, 11);},
    convertUnicode12ToBinary: function(x) { return convertCharCodeToBinary(x, 12);},
    convertBinaryToUnicode12: function(x) { return convertBinaryToCharCode(x, 12);},
    convertUnicode13ToBinary: function(x) { return convertCharCodeToBinary(x, 13);},
    convertBinaryToUnicode13: function(x) { return convertBinaryToCharCode(x, 13);},
    convertUnicode14ToBinary: function(x) { return convertCharCodeToBinary(x, 14);},
    convertBinaryToUnicode14: function(x) { return convertBinaryToCharCode(x, 14);},
    convertUnicode15ToBinary: function(x) { return convertCharCodeToBinary(x, 15);},
    convertBinaryToUnicode15: function(x) { return convertBinaryToCharCode(x, 15);},
    convertUnicode16ToBinary: function(x) { return convertCharCodeToBinary(x, 16);},
    convertBinaryToUnicode16: function(x) { return convertBinaryToCharCode(x, 16);},
};